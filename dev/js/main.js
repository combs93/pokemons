/* frameworks */
//=include ../../node_modules/svg4everybody/dist/svg4everybody.min.js
//=include ../../node_modules/swiper/swiper-bundle.js

// the main code
svg4everybody();

// burger
if (document.querySelectorAll('.header__burger').length > 0) {
    document.querySelector('.header__burger').onclick = function(e) {
        this.classList.toggle('is-active');
        this.closest('.header').classList.toggle('menu-is-active');
        this.closest('.l-wrapper').classList.toggle('menu-is-active');
    }
}

// header scroll

if (document.querySelectorAll('.header').length > 0) {
    window.addEventListener('scroll', function(){
        if(window.scrollY > 10){
            document.querySelector('.header').classList.add('is-scrolled');
        }
        else {
            document.querySelector('.header').classList.remove('is-scrolled');
        }
    });
}


// swiper
if (document.querySelectorAll('.carousel-section__container').length > 0) {
    const swiper = new Swiper('.carousel-section__container', {
        loop: false,
        speed: 800,
        spaceBetween: 40,
        pagination: {
            el: '.carousel-section__pagination',
            clickable: true,
        },
        breakpoints: {
            768: {
                spaceBetween: 50
            },
            992: {
                spaceBetween: 100
            }
        },
    });
}
// achievement carousel
if (document.querySelectorAll('.js-achievement-carousel').length > 0) {
    const swiper = new Swiper('.js-achievement-carousel', {
        loop: false,
        speed: 800,
        spaceBetween: 20,
        navigation: {
            nextEl: '.achievements__item__icon__next',
            prevEl: '.achievements__item__icon__prev',
        },
        pagination: {
            el: '.achievements__item__icon__pagination',
            clickable: true,
        }
    });
}
// collect section carousel
if (document.querySelectorAll('.collect-section__container').length > 0) {
    const collectSwiper = new Swiper('.collect-section__container', {
        loop: true,
        speed: 800,
        spaceBetween: 15,
        slidesPerView: 'auto',
        centeredSlides: true,
        pagination: {
            el: '.collect-section__pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.collect-section__next',
            prevEl: '.collect-section__prev',
        },
        breakpoints: {
            576: {
                spaceBetween: 30,
                slidesPerView: 'auto',
                centeredSlides: true
            },
            768: {
                spaceBetween: 30,
                slidesPerView: 2,
                centeredSlides: false,
                loop: false
            },
            992: {
                spaceBetween: 30,
                slidesPerView: 3,
                centeredSlides: false,
                loop: false
            }
        },
    });
}

// modals
if (document.querySelectorAll('.js-overlay-modal').length > 0) {
    document.addEventListener('DOMContentLoaded', function() {
        let modalButtons = document.querySelectorAll('.js-open-modal'),
            overlay      = document.querySelector('.js-overlay-modal'),
            closeButtons = document.querySelectorAll('.js-modal-close');
        modalButtons.forEach(function(item){
            item.addEventListener('click', function(e) {
                e.preventDefault();
                let modalId = this.getAttribute('data-modal'),
                    modalElem = document.querySelector('.modal-custom[data-modal="' + modalId + '"]');
                modalElem.classList.add('active');
                if (modalId == '14') {
                    // document.getElementById('magic_box_audio').play();
                    document.querySelector('.modal-magic__inner').classList.add('animation-started');
                }
                overlay.classList.add('active');
                document.querySelectorAll('body')[0].classList.add('open');


                let div = document.createElement('div');
                div.style.overflowY = 'scroll';
                div.style.width = '50px';
                div.style.height = '50px';
                document.body.append(div);
                let scrollWidth = div.offsetWidth - div.clientWidth;

                // div.remove();
                div.parentNode.removeChild(div);

                document.body.style.overflow = 'hidden';
                document.body.style.paddingRight = scrollWidth + 'px';
            });
        });
        closeButtons.forEach(function(item){
            item.addEventListener('click', function(e) {
                let parentModal = this.closest('.modal-custom');
                parentModal.classList.remove('active');
                overlay.classList.remove('active');
                document.querySelectorAll('body')[0].classList.remove('open');

                document.body.style.overflow = '';
                document.body.style.padding = '';
            });
        });
        document.body.addEventListener('keyup', function (e) {
            const key = e.keyCode;
            if (key == 27) {
                document.querySelector('.modal-custom.active').classList.remove('active');
                document.querySelector('.modal-overlay').classList.remove('active');
                document.querySelectorAll('body')[0].classList.remove('open');

                document.body.style.overflow = '';
                document.body.style.padding = '';
            }
        }, false);
        overlay.addEventListener('click', function() {
            document.querySelector('.modal-custom.active').classList.remove('active');
            this.classList.remove('active');
        });
    });
}


//DROPDOWN

var dropButton = document.querySelectorAll(".drop-custom__btn");
var dropWrap = document.querySelectorAll(".drop-custom");

for (var i = 0; i < dropButton.length; i++) {
    (function(i) {
        dropButton[i].onclick = function() {
            dropWrap[i].classList.toggle('visible');
        };
    })(i);
}

//DROPDOWN END

//MORE

var moreButton = document.querySelectorAll(".js-more-btn");
var moreWrap = document.querySelectorAll(".js-more-content");

for (var i = 0; i < moreButton.length; i++) {
    (function(i) {
        moreButton[i].onclick = function() {
            moreWrap[i].classList.toggle('visible');
            document.querySelector('.mobile-navigation').classList.toggle('is-opened');
        };
    })(i);
}

//MORE END



// new edit bid
if (document.querySelectorAll('.edit-bid').length > 0) {
    document.querySelector('.edit-bid').onclick = function(e) {
        this.closest('.h-tails__inner').classList.add('modal-is-active');
    }
}
if (document.querySelectorAll('.js-close-modal-bid').length > 0) {
    document.querySelector('.js-close-modal-bid').onclick = function(e) {
        this.closest('.h-tails__inner').classList.remove('modal-is-active');
    }
}



// flipping coin
if (document.querySelectorAll('.js-start-flip-win').length > 0) {
    document.querySelector('.js-start-flip-win').onclick = function(e) {
        this.closest('.h-tails__inner').classList.add('flipping-h');
    }
}
if (document.querySelectorAll('.js-start-flip-loose').length > 0) {
    document.querySelector('.js-start-flip-loose').onclick = function(e) {
        this.closest('.h-tails__inner').classList.add('flipping-t');
    }
}
if (document.querySelectorAll('.js-start-flip-winn').length > 0) {
    document.querySelector('.js-start-flip-winn').onclick = function(e) {
        this.closest('.h-tails__inner').classList.add('flipping-h');
    }
}
if (document.querySelectorAll('.js-start-flip-loosee').length > 0) {
    document.querySelector('.js-start-flip-loosee').onclick = function(e) {
        this.closest('.h-tails__inner').classList.add('flipping-t');
    }
}


//DROPDOWN

var dropButton45 = document.querySelectorAll(".js-select-custom-btn");
var dropWrap45 = document.querySelectorAll(".js-select-custom");

for (var i = 0; i < dropButton45.length; i++) {
    (function(i) {
        dropButton45[i].onclick = function() {
            dropWrap45[i].classList.toggle('visible');
        };
    })(i);
}

//DROPDOWN END